import { createStore } from 'vuex'
import data from '../../public/content/data.json'

export default createStore({
  state: {
    roles: data,
    search: "",
    filter: ""
  },
  mutations: {
    setRole(state, payload){
      state.roles.push(payload);
    },
    deleteRole(state, payload){
      const index = state.roles.findIndex(role => role.id == payload)
      state.roles.splice(index, 1)
    },
    setSearch(state, payload){
      state.search = payload
    },
    setFilter(state, payload){
      state.filter = payload
    }
  },
  actions: {
    setRole({ commit }, role){
      commit('setRole', role)
    },
    deleteRole({ commit }, roleId){
      commit('deleteRole', roleId)
    },
    setSearch({ commit }, data){
      commit('setSearch', data)
    },
    setFilter({ commit }, data){
      commit('setFilter', data)
    }
  }
})
